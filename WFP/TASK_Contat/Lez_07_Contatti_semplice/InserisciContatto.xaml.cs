﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Lez_07_Contatti_semplice.Controllers;
using Lez_07_Contatti_semplice.Models;

namespace Lez_07_Contatti_semplice
{
    /// <summary>
    /// Logica di interazione per InserisciContatto.xaml
    /// </summary>
    public partial class InserisciContatto : Window
    {
        public InserisciContatto()
        {
            InitializeComponent();
        }

        private void Salva_Click(object sender, RoutedEventArgs e)
        {
            Contatto temp = new Contatto()
            {
                Nome = inNome.Text,
                Cognome =inCognome.Text,
                Telefono = inTelefono.Text
            };

            ContattoController contattoController = new ContattoController();
            if (contattoController.InserisciContatto(inNome.Text, inCognome.Text, inTelefono.Text))
            {
                MessageBox.Show("Inserimento avvenuto con successo", "Tutto ok",
                    MessageBoxButton.OK, MessageBoxImage.Information);
                this.Close();
            }
            else
            {
                MessageBox.Show("Errore nell'inserimento", "si è rotto tutto",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
