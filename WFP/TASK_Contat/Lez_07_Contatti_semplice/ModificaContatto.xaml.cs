﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Lez_07_Contatti_semplice.Controllers;
using Lez_07_Contatti_semplice.Models;

namespace Lez_07_Contatti_semplice
{
    /// <summary>
    /// Logica di interazione per ModificaContatto.xaml
    /// </summary>
    public partial class ModificaContatto : Window
    {
        private Contatto contatto;

        public ModificaContatto(Contatto cont)
        {
            InitializeComponent();

            contatto = cont;
            inNome.Text = cont.Nome;
            inCognome.Text = cont.Cognome;
            inTelefono.Text = cont.Telefono;
        }

        private void Salva_Click(object sender, RoutedEventArgs e)
        {
            ContattoController gestore = new ContattoController();
            if (gestore.ModificaContatto(contatto.Id, inNome.Text, inCognome.Text, inTelefono.Text))
            {
                MessageBox.Show("Modifica Effettuata", "tutto ok", MessageBoxButton.OK, MessageBoxImage.Information);
                this.Close();
            }
            else
                MessageBox.Show("Modifica Non valida", "Si è rotto tutto", MessageBoxButton.OK, MessageBoxImage.Error);
        }
        private void Elimina_Click(object sender, RoutedEventArgs e)
        {
            ContattoController gestore = new ContattoController();
            if (gestore.EliminaContatto(contatto.Id))
            {
                MessageBox.Show("Cancellazione effettuata", "tutto ok", MessageBoxButton.OK, MessageBoxImage.Information);
                this.Close();
            }
            else
                MessageBox.Show("Cancellazione non andata a buon fine", "Si è rotto tutto", MessageBoxButton.OK, MessageBoxImage.Error);
        }
    }
}
