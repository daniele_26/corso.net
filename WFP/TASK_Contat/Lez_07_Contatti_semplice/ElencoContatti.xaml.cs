﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Lez_07_Contatti_semplice.Controllers;
using Lez_07_Contatti_semplice.Models;

namespace Lez_07_Contatti_semplice
{
    /// <summary>
    /// Interaction logic for ElencoContatti.xaml
    /// </summary>
    public partial class ElencoContatti : Window
    {
        public ElencoContatti()
        {
            InitializeComponent();
            LeggiValoriDb();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            InserisciContatto Inserimento = new InserisciContatto();
            Inserimento.ShowDialog();
            LeggiValoriDb();
        }

        private void LeggiValoriDb()
        {
            ContattoController gestore = new ContattoController();
            List<Contatto> elenco = gestore.VisualizzaTuttiContatti();
            lvContatti.ItemsSource = elenco;
        }

        private void lvContatti_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Contatto cont = (Contatto)(sender as ListView).SelectedItem;
            ModificaContatto modifica = new ModificaContatto(cont);
            modifica.ShowDialog();
            LeggiValoriDb();
        }
    }
}
