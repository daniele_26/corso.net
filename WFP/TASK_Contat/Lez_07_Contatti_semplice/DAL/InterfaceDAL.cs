﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lez_07_Contatti_semplice.DAL
{
    public interface InterfaceDAL<T>
    {
        List<T> RealAll();

        bool Insert(T item);

        bool Modify(T item);

        bool Delete(int varId);
    }
}
