﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using Lez_07_Contatti_semplice.Models;
using Microsoft.Extensions.Configuration;

namespace Lez_07_Contatti_semplice.DAL
{
    public class ContattoDAL : InterfaceDAL<Contatto>
    {
        private string connectionString;

        public ContattoDAL(IConfiguration config)
        {
            if (connectionString == null)
            {
                connectionString = config.GetConnectionString("DatabaseLocale");
            }
        }

        public List<Contatto> RealAll()
        {
            List<Contatto> elencoTemp = new List<Contatto>();

            using (SqlConnection connessione = new SqlConnection(connectionString))
            {
                string query = "SELECT contattoID, nome, cognome, telefono FROM Contatti";
                SqlCommand comm = new SqlCommand(query, connessione);
                connessione.Open();
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    Contatto temp = new Contatto()
                    {
                        Id = Convert.ToInt32(reader[0]),
                        Nome = reader[1].ToString(),
                        Cognome = reader[2].ToString(),
                        Telefono = reader[3].ToString()
                    };
                    elencoTemp.Add(temp);
                }
            }
            return elencoTemp;
        }

        public bool Insert(Contatto c)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                SqlCommand comm = new SqlCommand();
                comm.Connection = conn;
                comm.CommandText = $"INSERT INTO Contatti(nome, cognome, telefono) VALUES(@varNome, @varCognome, @varTelefono)";
                comm.Parameters.AddWithValue("@varNome", c.Nome);
                comm.Parameters.AddWithValue("@varCognome", c.Cognome);
                comm.Parameters.AddWithValue("@varTelefono", c.Telefono);
                conn.Open();

                if (comm.ExecuteNonQuery() > 0)
                    return true;
            }
            return false;
        }

        public bool Modify(Contatto cont)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                SqlCommand comm = new SqlCommand();
                comm.Connection = conn;
                comm.CommandText = "UPDATE Contatti SET nome = @varNome, cognome = @varCognome, telefono = @varTelefono WHERE contattoID = @varId";
                comm.Parameters.AddWithValue("@varNome", cont.Nome);
                comm.Parameters.AddWithValue("@varCognome", cont.Cognome);
                comm.Parameters.AddWithValue("@varTelefono", cont.Telefono);
                comm.Parameters.AddWithValue("@varId", cont.Id);

                conn.Open();
                if (comm.ExecuteNonQuery() > 0)
                    return true;
            }
            return false;
        }

        public bool Delete(int varId)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                SqlCommand comm = new SqlCommand();
                comm.Connection = conn;
                comm.CommandText = "DELETE FROM Contatti WHERE contattoID = @varId";
                comm.Parameters.AddWithValue("@varId", varId);

                conn.Open();
                if (comm.ExecuteNonQuery() > 0)
                    return true;
            }
            return false;
        }
    }
}
