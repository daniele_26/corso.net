﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows;
using Lez_07_Contatti_semplice.DAL;
using Lez_07_Contatti_semplice.Models;
using Microsoft.Extensions.Configuration;

namespace Lez_07_Contatti_semplice.Controllers
{
    public class ContattoController
    {
        private static IConfiguration configuration;

        public ContattoController()
        {
            if (configuration == null)
            {
                var builder = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json", false, false);
                configuration = builder.Build();
            }
        }

        public bool InserisciContatto(string varNome, string varCognome, string varTelefono)
        {
            Contatto temp = new Contatto()
            {
                Nome = varNome,
                Cognome = varCognome,
                Telefono = varTelefono
            };

            ContattoDAL contattoDal = new ContattoDAL(configuration);
            return contattoDal.Insert(temp);
        }

        public List<Contatto> VisualizzaTuttiContatti()
        {
            ContattoDAL contattoDal = new ContattoDAL(configuration);
            return contattoDal.RealAll();
        }

        public bool ModificaContatto(int varId, string varNome, string varCognome, string varTelefono)
        {
            Contatto temp = new Contatto()
            {
                Id = varId,
                Nome = varNome,
                Cognome = varCognome,
                Telefono = varTelefono
            };
            
            ContattoDAL contattoDal = new ContattoDAL(configuration);
            return contattoDal.Modify(temp);
        }

        public bool EliminaContatto(int varId)
        {
            ContattoDAL contattoDal = new ContattoDAL(configuration);
            return contattoDal.Delete(varId);
        }
    }
}
