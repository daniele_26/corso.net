﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lez_07_Contatti_semplice.Models
{
    public class Contatto
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Cognome { get; set; }
        public string Telefono { get; set; }

        public override string ToString()
        {
            return $"{Nome} - {Cognome} - {Telefono}";
        }
    }
}
