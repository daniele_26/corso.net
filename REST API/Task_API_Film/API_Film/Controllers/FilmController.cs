﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API_Film.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace API_Film.Controllers
{
    [Route("api/film")]
    [ApiController]
    public class FilmController :Controller
    {
        private readonly db_FilmContext _context = new db_FilmContext();

        [HttpGet]
        public ActionResult<IEnumerable<Film>> GetAllFilms() => Ok(_context.Films.ToListAsync().Result);


        [HttpGet("{varId}")]
        public ActionResult GetFilmById(int varId)
        {
            return Ok(_context.Films.SingleOrDefault(i => i.FilmId == varId));
        } 

        [HttpPost("insert")]
        public ActionResult InsertFilm(Film obj)
        {
            try
            {
                _context.Films.Add(obj);
                _context.SaveChangesAsync();
                return Ok("Successo");
            }
            catch (Exception e)
            {
                return Ok("Problemi nell'inserimento");
            }
        }

        [HttpDelete("{varId}")]
        public ActionResult DeleteFilm(int varId)
        {
            try
            {
                var temp = _context.Films.SingleOrDefault(i => i.FilmId == varId);
                _context.Films.Remove(temp);
                if (_context.SaveChanges() > 0)
                    return Ok("Successo");
            }
            catch (Exception e)
            {
                return Ok("Problemi nella cancellazione");
            }
            return Ok("Problemi nella cancellazione");
        }

        [HttpPut("{varId}")]
        public ActionResult ModifyFilm(Film obj, int varId)
        {
            try
            {
                var temp = _context.Films.SingleOrDefault(i => i.FilmId == varId);
                temp.Titolo = obj.Titolo;
                temp.Immagine = obj.Immagine;
                temp.Descrizione = obj.Descrizione;
                temp.DataRilascio = obj.DataRilascio;
                temp.Durata = obj.Durata;
                temp.Incasso = obj.Incasso;
                temp.Produttore = obj.Produttore;
                temp.Regista = obj.Regista;
                if (_context.SaveChanges() > 0)
                    return Ok("Successo");
            }
            catch (Exception e)
            {
                return Ok("Problemi nella modifica");
            }
            return Ok("Problemi nella modifica");
        }
    }
}
