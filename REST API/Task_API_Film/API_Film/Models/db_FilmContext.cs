﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace API_Film.Models
{
    public partial class db_FilmContext : DbContext
    {
        public db_FilmContext()
        {
        }

        public db_FilmContext(DbContextOptions<db_FilmContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Film> Films { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Server=LAPTOP-V2JOQPIF; Database=db_Film; User Id=sharpuser;Password=Cicciopasticcio;MultipleActiveResultSets=true;Encrypt=false;TrustServerCertificate=false");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "Latin1_General_CI_AS");

            modelBuilder.Entity<Film>(entity =>
            {
                entity.ToTable("Film");

                entity.Property(e => e.FilmId).HasColumnName("filmID");

                entity.Property(e => e.DataRilascio)
                    .IsRequired()
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasColumnName("dataRilascio");

                entity.Property(e => e.Descrizione)
                    .IsRequired()
                    .HasColumnType("text")
                    .HasColumnName("descrizione");

                entity.Property(e => e.Durata).HasColumnName("durata");

                entity.Property(e => e.Immagine)
                    .IsRequired()
                    .HasMaxLength(500)
                    .IsUnicode(false)
                    .HasColumnName("immagine");

                entity.Property(e => e.Incasso).HasColumnName("incasso");

                entity.Property(e => e.Produttore)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("produttore");

                entity.Property(e => e.Regista)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("regista");

                entity.Property(e => e.Titolo)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false)
                    .HasColumnName("titolo");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
