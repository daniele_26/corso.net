﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Mongo_API_Prodotti_TASK.Models;
using MongoDB.Bson;
using MongoDB.Driver;

namespace Mongo_API_Prodotti_TASK.DAL
{
    public class CategoryRepo : IRepo<Category>
    {
        private readonly IMongoCollection<Category> categories;

        public CategoryRepo(string strConnection, string strDatabase)
        {
            var client = new MongoClient(strConnection);
            var db = client.GetDatabase(strDatabase);
            categories = db.GetCollection<Category>("Categories");
        }

        public IEnumerable<Category> GetAll() => categories.Find(FilterDefinition<Category>.Empty).ToList();

        public Category GetById(ObjectId varId) => categories.Find(i => i.DocumentId == varId).FirstOrDefault();

        public bool Insert(Category t)
        {
            Category temp = categories.Find(i => i.Codice == t.Codice).FirstOrDefault();
            if (temp == null)
            {
                t.Codice = Guid.NewGuid().ToString();
                categories.InsertOne(t);
                return true;
            }
            return false;
        }

        public bool Delete(ObjectId varId)
        {
            Category temp = categories.Find(i => i.DocumentId == varId).FirstOrDefault();
            if (temp != null)
            {
                var result = categories.DeleteOne(i => i.DocumentId == varId);
                if (result.IsAcknowledged && result.DeletedCount > 0)
                    return true;
            }
            return false;
        }

        public bool Update(Category t)
        {
            Category temp = categories.Find(i => i.DocumentId == t.DocumentId).FirstOrDefault();
            if (temp != null)
            {
                temp.Titolo = t.Titolo ?? temp.Titolo;
                temp.Descrizione = t.Descrizione ?? temp.Descrizione;
                temp.Scaffale = t.Descrizione ?? t.Descrizione;
                temp.Codice = t.Codice ?? temp.Codice;

                var result = categories.ReplaceOne(i=> i.DocumentId == t.DocumentId, temp);
                if (result.IsAcknowledged && result.ModifiedCount > 0)
                    return true;
            }
            return false;
        }

        public Category FindByTitle(string title) => categories.Find(i => i.Titolo == title).FirstOrDefault();
    }
}
