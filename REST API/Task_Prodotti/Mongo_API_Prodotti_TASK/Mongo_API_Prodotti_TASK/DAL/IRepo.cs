﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;

namespace Mongo_API_Prodotti_TASK.DAL
{
    public interface IRepo<T>
    {
        IEnumerable<T> GetAll();
        T GetById(ObjectId varId);
        bool Insert(T t);
        bool Delete(ObjectId varId);
        bool Update(T t);
    }
}
