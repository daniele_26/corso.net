if(localStorage.getItem("chat_nickname") == null)
    localStorage.setItem("chat_nickname", JSON.stringify([]));

let nickname = JSON.parse(localStorage.getItem("chat_nickname"));
stampaChat();

function login(){
    var nickname = $("#nicknameField").val();
    localStorage.setItem("chat_nickname", JSON.stringify(nickname));
    $(window.location).attr('href', 'HOME.html');

}

function logout(){
    localStorage.clear();
    $(window.location).attr('href', 'HOME.html');
}

function stampaChat(){
    $.ajax(
        {
            url: "https://localhost:44378/api/Messaggi",
            method: "GET",
            success: function(response){
                let contenuto = "";
                for(let item of response)
                {
                    let orario = item.orario.hours+":"+item.orario.minutes+":"+item.orario.seconds;
          
                    contenuto += `
                    <a class="list-group-item list-group-item-action flex-column align-items-start">
                        <div class="d-flex w-100 justify-content-between">
                        <h5 class="mb-4"><i class="fa fa-user" aria-hidden="true"></i>${item.nomeUtente}</h5>
                        <small class="text-muted">${orario}</small>
                        </div>
                        <p style="overflow-wrap: anywhere;" class="mb-1">${item.messaggio}</p>
                    </a>
                    
                    `
                    ;
                }
                $("#itemContent").html(contenuto);
            }
        }
    )
}



function inviaMessaggio(){
    let username = nickname;
 
    let testo = $("#messaggio").val();

    let messaggio = {
        nomeUtente: username,
  
        messaggio: testo,
    }

    $.ajax(
        {
            url: "https://localhost:44378/api/Messaggi/insert",
            method: "POST",
            data: JSON.stringify(messaggio),
            dataType: "json",
            contentType: "application/json",
            success: function(risposta){
                switch(risposta.result){
                    case "success":
                        $("#messaggio").val("");
                        $("#messaggio").focus();
                        
                        break;
                    case "error":
                        alert("Errore: " + risposta.description);
                        break;
                }
            },
            errore: function(errore){
                console.log(errore);
                alert("Errore");
            }
        }
    );
}

$(document).ready(
    function(){
        $("#welcome").html(`<h1 class="font-weight-bold color-title">Benvenuto ${nickname}</h1>`);
        let autoUpdate = setInterval(function(){
            stampaChat();
        },100);

        console.log(localStorage.getItem("chat_nickname"));
        setTimeout(function(){
            var page = window.location.pathname.split("/").pop();
            if(localStorage.getItem("chat_nickname") == "[]" && page == "chat.html")
            {
                 $(window.location).attr('href', 'HOME.html');
            }else if(localStorage.getItem("chat_nickname") != "[]" && page == "HOME.html")
            {
                $(window.location).attr('href', 'CHAT.html');
            }
        },1000);
    }   
)