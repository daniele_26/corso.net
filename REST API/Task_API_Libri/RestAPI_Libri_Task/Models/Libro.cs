﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestAPI_Libri_Task.Models
{
    public class Libro
    {
        public int Id { get; set; }
        public string Titolo { get; set; }
        public string Descrizione { get; set; }
        public string Autore { get; set; }
        public string Isbn { get; set; }
        public int Quantita { get; set; }
    }
}
