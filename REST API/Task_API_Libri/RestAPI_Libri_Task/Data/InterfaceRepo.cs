﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestAPI_Libri_Task.Data
{
    public interface InterfaceRepo<T>
    {
        IEnumerable<T> GetAll();
        T GetById(int varId);
        bool Insert(T obj);
        bool Delete(int varId);
        bool Update(T obj);
    }
}
