﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Gestionale.Classi {
    public class Piatto {
        public int Codice { get; set; }

        public string Nome { get; set; }

        public float prezzo { get; set; }

        public Piatto() { 


        }

        public Piatto(int varCodice, string varNome , float varprezzo) {

            Codice = varCodice;
            Nome = varNome;
            prezzo = varprezzo;
        }

        public override string ToString()
        {
            return $"ID: {Codice}\n" +
                   $"Piatto:{Nome}\n" +
                   $"Prezzo:{prezzo}";
        }
    }
}
