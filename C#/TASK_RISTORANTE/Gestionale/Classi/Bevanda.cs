﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Gestionale.Classi {
    public class Bevanda {
        public int Codice { get; set; }
        public string marca { get; set; } 
        public float prezzo { get; set; }

        public Bevanda() { 
        
        }

        public Bevanda(int varCodice1, string varmarca1, float varprezzo1) 
        {
            Codice = varCodice1;
            marca = varmarca1;
            prezzo = varprezzo1;
        }

        public override string ToString() {
            return $"ID: {Codice}\n" +
                   $"Bevanda:{marca}\n" +
                   $"Prezzo:{prezzo}"; ;
        }
    }
}
