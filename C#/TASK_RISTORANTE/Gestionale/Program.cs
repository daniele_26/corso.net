﻿using Gestionale.Classi;
using System;
using System.Collections.Generic;
using System.Text;

namespace Gestionale {

    /*Creare un software per la gestione del menu di UN ristorante,
        questo avrà due categorie di articolo: bevande, piatti.

            Creare i metodi in grado di:
            - Inserire una bevanda o un piatto
            - Stampare tutte le bevande o piatti (a scelta)
            -> Contare tutte le bevande o piatti (a scelta)

            Ogni articolo avrà almeno:
            - Codice
            - Nome
            - Prezzo
            */
    public class Program {
        static void Main(string[] args) {
           

            bool menu = true;
            //Lista Piatti 
            List<Piatto> elenco1 = new List<Piatto>(); 

            elenco1.Add(new Piatto (001,"Pasta al tonno",15f));
            elenco1.Add(new Piatto (002,"Carbonara",25.5f));
            elenco1.Add(new Piatto (003,"Pizza Margherita", 3.5f));

            //Lista Bevande
            List<Bevanda> elenco2 = new List<Bevanda>();
            elenco2.Add(new Bevanda(001, "Coca-Cola", 1f));
            elenco2.Add(new Bevanda(002, "Uliveto", 1.5f));
            elenco2.Add(new Bevanda(003, "Fanta", 1.5f));

            while (menu) {

            Console.WriteLine("-------------------Benventuo al Risottoante-----------------");
            Console.WriteLine("]]]]] Premi '1' inserisci i dati di un Piatto o Bevanda [[[[[");
            Console.WriteLine("]]]]] Premi '2' per visuallizare Piatti e Bevande       [[[[[");
            Console.WriteLine("]]]]] Premi '3' per vedere le info del risorante        [[[[[");
            Console.WriteLine("]]]]] Premi '4' per uscire dal menú                     [[[[[");
            Console.WriteLine("------------------------------------------------------------");

                string caso = Console.ReadLine();
                int verifica = 0 ;
                bool test = true;


            //blocco per le eccezioni input 
            try {

                verifica = Convert.ToInt32(caso);

            }
            catch (FormatException e) {

                Console.WriteLine("Valore 'errato' , Riprova");

                test = false;
            }

            if (test) 
                switch (verifica) {
                    //inserimento bevanda o piatto 
                    case 1:
                            Console.WriteLine("---  Inserisci  dati  ---\n" +
                                               "Premi 1 per un Piatto\n" +
                                               "Premi 2 per una Bevanda\n");

                            int i = Convert.ToInt32(Console.ReadLine());
                            int Codice1, Codice2;
                            string Nome1, marca;
                            float prezzo1, prezzo2;
                            //Inserimento caso Piatto
                            if( i==1 ) {

                                Console.WriteLine("Inserisci il Codice: ");

                                Codice1 =Convert.ToInt32(Console.ReadLine());
                                
                                Console.WriteLine("Inserisci Nome Piatto: ");
                                Nome1 = Console.ReadLine();

                                Console.WriteLine("Inserisci Prezzo Piatto: ");
                                prezzo1 = Convert.ToInt32(Console.ReadLine());

                                Piatto temp = new Piatto(Codice1, Nome1, prezzo1);

                                elenco1.Add(temp);

                                Console.WriteLine("Inserimento riuscito");
                            }
                            //Inserimento caso Bevanda
                            else if (i == 2){
                                Console.WriteLine("Inserisci il Codice: ");

                                Codice2 = Convert.ToInt32(Console.ReadLine());

                                Console.WriteLine("Inserisci Nome Benvanda: ");
                                marca = Console.ReadLine();

                                Console.WriteLine("Inserisci Prezzo Bevanda: ");
                                prezzo2 = Convert.ToInt32(Console.ReadLine());

                                Bevanda temp = new Bevanda(Codice2, marca, prezzo2);

                                elenco2.Add(temp);

                                Console.WriteLine("Inserimento riuscito");
                            }

                            else {

                                Console.WriteLine(" --Valore erratto 'Riprova'-- ");
                                return;
                            }
                            
                            Console.WriteLine("Operazione effettuata con successo!\n");
                            break;


                    //Stampa tutte le bevande o piatti
                    case 2:
                     Console.WriteLine("---------------------------------");
                    foreach (Piatto item in elenco1) {
                     Console.WriteLine(item);
                    }

                    Console.WriteLine("---------------------------------");
                            foreach (Bevanda item in elenco2) {
                                Console.WriteLine(item);
                            }
                    Console.WriteLine("---------------------------------");
                           return;

                    //Info del ristorante ("informazione fuori traccia")
                    case 3:
                        Console.WriteLine("------Indririzzo ed orari di apertura------");
                        Console.WriteLine("]]]]]       via Margherita n1889      [[[[[");
                        Console.WriteLine("]]]]]   Dal Lunedi al Vnerdi '09-18'  [[[[[");
                        return;
                    //Uscita dal programma
                    case 4:
                            menu = !menu;
                        break;

.                  default:
                        Console.WriteLine("-----Valore inserito 'Errato' Riprova-----");
                        break;
                }
                 else {

                    Console.WriteLine("Valore errato ;( ");

                 }
            } 

        }
    }
}
