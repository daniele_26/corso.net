﻿using System;
using DB_ElencoCitta.Controllers;

namespace DB_ElencoCitta
{
    class Program
    {
        static void Main(string[] args)
        {
            CittaController gestore = new CittaController();
            
            //gestore.InserireCitta("Catanzaro", "CZ");
            
            gestore.StampaCitta();
            //gestore.EliminaCittaPerId(1);
            gestore.ModificaCittaPerId(2, "Frosinone", "FR");
            gestore.StampaCitta();
        }
    }
}
