﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskBoxOffice.Models;

namespace TaskBoxOffice.controller
{
    [Route("api/film")]
    [ApiController]
    public class ControllerFilm : Controller
    {
        [HttpGet("lista")]
        public ActionResult<IEnumerable<Film>> GetAllFilm()
        {
            var elenco = new List<Film>();
            try
            {
                using(var context = new db_BoxOfficeContext())
                {
                    elenco = context.Films.ToList();
                }
            }catch(Exception ex)
            {
                return Ok("Problema");
            }
            return Ok(elenco);
        }
        [HttpPost]
        public ActionResult InsertFilm(Film obj)
        {
            try
            {
                using (var context = new db_BoxOfficeContext())
                {
                    context.Films.Add(obj);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                return Ok("Problema");
            }
            return Ok();
        }
        [HttpPut("{varId}")]
        public ActionResult UpdateFilm(int varId,Film obj)
        {
            try
            {
                using (var context = new db_BoxOfficeContext())
                {
                    context.Films
                        .Where(o => o.FilmId == varId)
                        .Select(k => new Film() { Titolo = obj.Titolo, DataUscita = obj.DataUscita, Descrizione = obj.Descrizione,Durata = obj.Durata,Img=obj.Img,Incasso=obj.Incasso,Produttore=obj.Produttore,Regista=obj.Regista ,FilmId = k.FilmId });
                    context.SaveChanges();

                }
            }
            catch (Exception ex)
            {
                return Ok("PROBLEMA");
            }
            return Ok("Successo");
        }
        [HttpDelete("{varId}")]
        public ActionResult DeleteContatto(int varId)
        {

            try
            {
                using (var context = new db_BoxOfficeContext())
                {
                    Film temp = context.Films.FirstOrDefault(d => d.FilmId == varId);
                    context.Films.Remove(temp);
                    context.SaveChanges();

                }
            }
            catch (Exception ex)
            {
                return Ok("PROBLEMA");
            }
            return Ok("Successo");
        }

    }
}
