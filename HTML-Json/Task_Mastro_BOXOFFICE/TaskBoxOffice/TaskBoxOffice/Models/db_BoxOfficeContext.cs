﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace TaskBoxOffice.Models
{
    public partial class db_BoxOfficeContext : DbContext
    {
        public db_BoxOfficeContext()
        {
        }

        public db_BoxOfficeContext(DbContextOptions<db_BoxOfficeContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Film> Films { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Server=DESKTOP-VULS57I\\SQLEXPRESS;Database=db_BoxOffice;User Id=sharpuser;Password=cicciopasticcio;Trusted_Connection=true;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "Latin1_General_CI_AS");

            modelBuilder.Entity<Film>(entity =>
            {
                entity.ToTable("Film");

                entity.Property(e => e.FilmId).HasColumnName("filmId");

                entity.Property(e => e.DataUscita)
                    .HasColumnType("date")
                    .HasColumnName("dataUscita");

                entity.Property(e => e.Descrizione)
                    .HasColumnType("text")
                    .HasColumnName("descrizione");

                entity.Property(e => e.Durata).HasColumnName("durata");

                entity.Property(e => e.Img)
                    .HasColumnType("text")
                    .HasColumnName("img");

                entity.Property(e => e.Incasso).HasColumnName("incasso");

                entity.Property(e => e.Produttore)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("produttore");

                entity.Property(e => e.Regista)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("regista");

                entity.Property(e => e.Titolo)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("titolo");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
