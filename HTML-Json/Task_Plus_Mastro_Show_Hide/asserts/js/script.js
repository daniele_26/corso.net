function inserisci() {
  let canzone = {
    id: calcolaId(),
    nome: $('#in_canzone').val(),
    cantante: $('#in_cantante').val(),
    anno: parseInt($('#in_anno').val()),
  };

  if (
    canzone.anno < 1600 ||
    canzone.nome.length == 0 ||
    canzone.cantante.length == 0
  ) {
    alert('Inserimento non valido');
    return;
  }

  if (isPresente(canzone) != -1) {
    alert('canzone gia presente!');
    return;
  } else {
    elenco.push(canzone);
    stampaLista();
    alert('Inserimento avvenuto!');
    localStorage.setItem('listaCanzoni', JSON.stringify(elenco));
  }
}

function calcolaId() {
  if (elenco.length != 0) {
    return elenco[elenco.length - 1].id + 1;
  } else return 1;
}

function stampaLista() {
  if (elenco.length != 0) {
    $('#contenutoTabella').html('');
    let content = '';
    for (let item of elenco) {
      let elem = `<tr>
                    <td>${item.nome}</td>
                    <td>${item.cantante}</td>
                    <td>${item.anno}</td>
                    <td>
                        <button type="button" class="btn btn-outline-danger btn-sm" onclick="cancella(${item.id})">
                            <i class="fa-solid fa-trash"></i>   
                        </button>
                        <button type="button" class="btn btn-outline-warning btn-sm" onclick="apriModale(${item.id})">
                            <i class="fa-solid fa-pencil"></i>
                        </button>
                    </td>
                </tr>`;
      content += elem;
    }
    $('#contenutoTabella').html(content);
  } else {
    $('#contenutoTabella').html('');
  }
}

function apriModale(varId) {
  for (let item of elenco) {
    if (item.id == varId) {
      $('#up_id').val(item.id);
      $('#up_titolo').val(item.nome);
      $('#up_cantante').val(item.cantante);
      $('#up_anno').val(item.anno);
    }
  }
  $('#modaleModifica').modal('show');
}

function aggiorna() {
  let upCanzone = {
    id: $('#up_id').val(),
    nome: $('#up_titolo').val(),
    cantante: $('#up_cantante').val(),
    anno: $('#up_anno').val(),
  };

  for (let item of elenco) {
    if (item.id == upCanzone.id) item = upCanzone;
  }
  localStorage.setItem('listaCanzoni', JSON.stringify(elenco));
  stampaLista();
  $('#modaleModifica').modal('hide');
}

function cancella(varId) {
  for (let [ind, item] of elenco.entries()) {
    if (item.id == varId) elenco.splice(ind, 1);
  }
  localStorage.setItem('listaCanzoni', JSON.stringify(elenco));
  stampaLista();
}

function isPresente(canzone) {
  for (let [index, item] of elenco.entries()) {
    if (item.nome == canzone.nome && item.cantante == canzone.nome)
      return index;
  }
  return -1;
}

function cancTutto() {
  elenco = [];
  localStorage.setItem('listaCanzoni', JSON.stringify(elenco));
  stampaLista();
}

function showTab1() {
  if (!$('#tab2').hasClass('d-none')) {
    $('#tab1').toggleClass('d-none');
    $('#tab2').toggleClass('d-none');
  }
}
function showTab2() {
  if (!$('#tab1').hasClass('d-none')) {
    $('#tab2').toggleClass('d-none');
    $('#tab1').toggleClass('d-none');
  }
}

if (localStorage.getItem('listaCanzoni') == null)
  localStorage.setItem('listaCanzoni', JSON.stringify([]));

let elenco = JSON.parse(localStorage.getItem('listaCanzoni'));
stampaLista();
