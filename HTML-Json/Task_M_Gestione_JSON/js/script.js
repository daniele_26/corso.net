function SHOW_C(prodotti, varCategoria, idTabella) {
  let cont = '';
  prodotti.forEach((i) => {
    if (i.categoria.toLowerCase() === varCategoria.toLowerCase()) {
      let elemt = `<tr>
          <td>${i.genere}</td>
          <td>${i.prodotto}</td>
          <td>${i.prezzo}</td>
          <td>${i.venduti}</td>
          </tr>`;
      cont += elemt;
    }
  });
  $(`#${idTabella}`).html(cont);
}

function SHOW_G(prodotti, genere, idTabella) {
  let cont = '';
  prodotti.forEach((i) => {
    if (i.genere.toLowerCase() === genere.toLowerCase()) {
      let elemt = `<tr>
            <td>${i.categoria}</td>
            <td>${i.prodotto}</td>
            <td>${i.prezzo}</td>
            <td>${i.venduti}</td>
            </tr>`;
      cont += elem;
    }
  });
  $(`#${idTabella}`).html(cont);
}

function SottoTrenta(prodotti) {
  let cont = '';
  prodotti.forEach((i) => {
    if (parseInt(i.prezzo) < 30) {
      let elemt = `<tr>
            <td>${i.genere}</td>
            <td>${i.prodotto}</td>
            <td>${i.prezzo}</td>
            <td>${i.venduti}</td>
            </tr>`;
      cont += elemt;
    }
  });
  $(`#contSottoTrenta`).html(cont);
}

function NumeroC (prodotti) {
  let noDoppioni = [];
  for (let i of prodotti) {
    let flag = false;
    for (let item of noDoppioni) {
      if (i.categoria == item) flag = true;
    }
    if (!flag) noDoppioni.push(i.categoria);
  }
  return noDoppioni.length;
}

function TOP5 (prodotti) {
  let TopV = prodotti.sort(Confronto).reverse().slice(0, 5);
  let cont = '';
  TopV.forEach((i) => {
    let elemt = `<tr>
            <td>${i.genere}</td>
            <td>${i.prodotto}</td>
            <td>${i.prezzo}</td>
            <td>${i.venduti}</td>
            </tr>`;
    cont += elemt;
  });
  $(`#contTopCinq`).html(cont);
}

function Tabelle(prodotti) {
  SHOW_C(prodotti, 'abbigliamento', 'contAbbigl');
  SHOW_C(prodotti, 'calzature', 'contCalz');
  SHOW_C(prodotti, 'borse', 'contBorse');
  SHOW_G(prodotti, 'uomo', 'contUomo');
  SHOW_G(prodotti, 'donna', 'contDonna');
  SottoTrenta(prodotti);
  TOP5 (prodotti);
  $('#numCat').html(NumeroC(prodotti));
}

function Confronto(a, b) {
  if (a.venduti < b.venduti) return -1;
  if (a.venduti > b.venduti) return 1;
  return 0;
}

// --------------------------------------------------------------------------------
// PRESA DEI DATI JSON dal host
function PresaProdotti() {
  $.ajax({
    url: 'https://jsonkeeper.com/b/75NU',
    method: 'GET',
    success: (resp) => {
      Tabelle(resp);
    },
    error: () => console.log('Errore'),
  });
}

$(document).ready(() => {
  PresaProdotti();
});
// --------------------------------------------------------------------------------