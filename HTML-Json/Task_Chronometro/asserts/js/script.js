function start() {
  if (crono == null) {
    crono = setInterval(() => {
      timer.setSeconds(timer.getSeconds() + 1);
      $('#timerText').html(
        `${format(timer.getHours())}:${format(timer.getMinutes())}:${format(
          timer.getSeconds()
        )}`
      );
    }, 1000);
  }
}

let crono = null;

function format(value) {
  return ('0' + value).slice(-2);
}

function stop() {
  clearInterval(crono);
  crono = null;
}

function reset() {
  stop();
  setZero(timer);
  $('#timerText').html(
    `${format(timer.getHours())}:${format(timer.getMinutes())}:${format(
      timer.getSeconds()
    )}`
  );
}

$('document').ready(() => {
  $('#btnStart').on('click', start);
  $('#btnStop').on('click', stop);
  $('#btnReset').on('click', reset);
});

function setZero(timer) {
  timer.setMilliseconds(0);
  timer.setSeconds(0);
  timer.setMinutes(0);
  timer.setHours(0);
}
let timer = new Date();
setZero(timer);
