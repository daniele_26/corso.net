const container = document.querySelector('.container');
// grab all the seats in the row that are not occupied
const seats = document.querySelectorAll('.row .seat:not(.occupied)');
const count = document.getElementById('count');
const total = document.getElementById('total');
const movieSelect = document.getElementById('movie');

populateUI();

let ticketPrice = +movieSelect.value;

// Save selected movie index and price
function setMovieData(movieIndex, moviePrice) {
  localStorage.setItem('selectedMovieIndex', movieIndex);
  localStorage.setItem('selectedMoviePrice', moviePrice);
}

// Update total and count
function updateSelectedCount() {
  const selectedSeats = document.querySelectorAll('.row .seat.selected');

  const seatsIndex = [...selectedSeats].map((seat) => [...seats].indexOf(seat));

  localStorage.setItem('selectedSeats', JSON.stringify(seatsIndex));

  const selectedSeatsCount = selectedSeats.length;

  count.innerText = selectedSeatsCount;
  total.innerText = selectedSeatsCount * ticketPrice;
}

// Get data from localstorage and populate UI
function populateUI() {
  const selectedSeats = JSON.parse(localStorage.getItem('selectedSeats'));

  if (selectedSeats !== null && selectedSeats.length > 0) {
    seats.forEach((seat, index) => {
      if (selectedSeats.indexOf(index) > -1) {
        seat.classList.add('selected');
      }
    });
  }

  const selectedMovieIndex = localStorage.getItem('selectedMovieIndex');

  if (selectedMovieIndex !== null) {
    movieSelect.selectedIndex = selectedMovieIndex;
  }
}

// Movie select event
movieSelect.addEventListener('change', (e) => {
  ticketPrice = +e.target.value;
  setMovieData(e.target.selectedIndex, e.target.value);
  updateSelectedCount();
});

// Seat click event
container.addEventListener('click', (e) => {
  if (
    e.target.classList.contains('seat') &&
    !e.target.classList.contains('occupied')
  ) {
    e.target.classList.toggle('selected');

    updateSelectedCount();
  }
});

  // Initial count and total set
  updateSelectedCount();


  $('select').on('load change', function() {
    if(document.getElementById('movie').value == "1") {
      $('.screen').css("background-image", "url('https://cdn.gelestatic.it/deejay/sites/2/2016/11/Pulp-Fiction-2-cop.jpg'");
    } else if(document.getElementById('movie').value == "2") {
      $('.screen').css("background-image", "url('https://www.artapartofculture.net/new/wp-content/uploads/2019/10/D6FF18AC-B461-4E19-93FA-A9812E03FE70-2652-0000013F8E290605.jpg'");
    } else if(document.getElementById('movie').value == "3") {
      $('.screen').css("background-image", "url('https://auralcrave.com/ezoimgfmt/i0.wp.com/auralcrave.com/wp-content/uploads/2018/10/inglorious-bastards.jpg'");
    } 
  });