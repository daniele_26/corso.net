﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Uni_Cinema.Models {
    public class Biglietto {
        [BsonId]
        [BsonElement("Id")]
        public ObjectId Id { get; set; }
        [BsonElement("nome Utente")]
        [Required]
        [StringLength(50)]
        public string Nome { get; set; }

        [BsonElement("cognome Utente")]
        [Required]
        [StringLength(50)]
        public string Cognome { get; set; }

        [BsonElement("email Utente")]
        [Required(ErrorMessage = "Il campo Email è obbligatorio")]
        [RegularExpression(@"^[^@\s] +@[^@\s]+\.[^@\s]+$", ErrorMessage = "L'Email deve rispettare il formato corretto")]
        [MaxLength(100)]
        public string Email { get; set; }


        [BsonElement("Quantitá")]
        [Required]
        public int  Quantitá { get; set; }


        [BsonIgnore]
        public Film FilmRIF { get; set; }

    }   
}

