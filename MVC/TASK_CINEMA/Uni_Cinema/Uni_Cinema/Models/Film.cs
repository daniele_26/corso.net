﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Uni_Cinema.Models {
    public class Film {

        [BsonId]
        [BsonElement("CodiceId")]
        public ObjectId idFilm { get; set; }

        [BsonElement("titolo")]
        [Required]
        public string Titolo { get; set; }

        [BsonElement("data&ora")]
        [Required]
        public DateTime DataOra { get; set; }

    }
}
