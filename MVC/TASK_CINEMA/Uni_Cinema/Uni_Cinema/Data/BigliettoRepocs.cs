﻿using Uni_Cinema.Models;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Uni_Cinema.Data {
    public class BigliettoRepocs : InterfaceRepo<Biglietto>
    {
        private IMongoCollection<Biglietto> utenti;
        private string connString;
        private string dbString;

        public BigliettoRepocs(string connString, string dbString) {
            var client = new MongoClient(connString);
            var db = client.GetDatabase(dbString);

            if (utenti == null)
                utenti = db.GetCollection<Biglietto>("Bigliettos");

            this.connString = connString;
            this.dbString = dbString;
        }

        public bool Delete(ObjectId id) {
            throw new NotImplementedException();
        }

        public IEnumerable<Biglietto> GetAll() {
            throw new NotImplementedException();
        }

        public Biglietto GetByEmail(string email) {
            return utenti.Find(i => i.Email == email).FirstOrDefault();
        }

        public Biglietto GetById(ObjectId id) {
            return utenti.Find(i => i.Id == id).FirstOrDefault();
        }

        public bool Insert(Biglietto t) {
            Biglietto temp = utenti.Find(i => i.Id == t.Id).FirstOrDefault();
            Biglietto emailCheck = GetByEmail(t.Email);
            if (temp == null && emailCheck == null) {
                utenti.InsertOne(t);
                return true;
            }
            return false;
        }

        public bool Update(Biglietto t) {
            throw new NotImplementedException();
        }
    }
    
}
