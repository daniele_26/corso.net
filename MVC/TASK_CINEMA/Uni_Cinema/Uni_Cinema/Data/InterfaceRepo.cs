﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Uni_Cinema.Data {
    interface InterfaceRepo<T> {
        IEnumerable<T> GetAll();
        T GetById(ObjectId id);
        bool Insert(T t);
        bool Update(T t);
        bool Delete(ObjectId id);
    }
}
