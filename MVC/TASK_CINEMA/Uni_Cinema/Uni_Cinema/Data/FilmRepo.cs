﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Uni_Cinema.Models;
  //Prendo dati dal db che ho inserito 
namespace Uni_Cinema.Data {
    public class FilmRepo : InterfaceRepo<Film> 
    {
        private IMongoCollection<Film> film;
        private string connString;
        private string dbString;

        public FilmRepo(string connString, string dbString) {
            var client = new MongoClient(connString);
            var db = client.GetDatabase(dbString);

            if (film == null)
                film = db.GetCollection<Film>("Film");

            this.connString = connString;
            this.dbString = dbString;
        }
        public bool Delete(ObjectId id) {
            throw new NotImplementedException();
        }

        public IEnumerable<Film> GetAll() {
            return film.Find(FilterDefinition<Film>.Empty).ToList();
        }

        public Film GetByCodice(string codice) {
            return film.Find(i => i.Titolo == codice).FirstOrDefault();
        }

        public Film GetById(ObjectId id) {
            return film.Find(i => i.idFilm == id).FirstOrDefault();
        }

        public bool Insert(Film t) {
            throw new NotImplementedException();
        }

        public bool Update(Film t) {
            throw new NotImplementedException();
        }
    }
}
