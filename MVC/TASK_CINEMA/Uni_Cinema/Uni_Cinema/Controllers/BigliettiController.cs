﻿using Uni_Cinema.Data;
using Uni_Cinema.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Uni_Cinema.Controllers {


    public class BigliettiController : Controller {

        public IActionResult Index() {
            return View();
        }
        public class UtenteController : Controller {
            private BigliettoRepocs _repobiglietti;
            private FilmRepo _repoFilm;

            public UtenteController(IConfiguration config) {
                if (_repobiglietti == null) {
                    bool isOnline = config.GetValue<bool>("IsOnline");

                    string connString = isOnline ?
                        config.GetValue<string>("MongoDbSettings:OnlineDb") :
                        config.GetValue<string>("MongoDbSettings:LocalDb");

                    string dbString = config.GetValue<string>("MongoDbSettings:NomeDb");

                    _repobiglietti = new BigliettoRepocs(connString, dbString);
                    _repoFilm = new FilmRepo(connString, dbString);

                }
            }

            [HttpPost]
            
            public IActionResult Login() {
                return View();
            }

            public IActionResult Prenotazione(string codice) {
                ObjectId idCorso = _repoCorso.GetByCodice(codice).Id;
                string currentUser = HttpContext.Session.GetString("isLogged");
                if (currentUser != null) {

                    Utente temp = _repoUtente.GetByEmail(currentUser);


                    if (_repoIscrizione.GetAll().Where(i => i.UtenteId == temp.Id) == null) {

                        Iscrizione newSub = new Iscrizione() {
                            CorsoId = idCorso,
                            UtenteId = temp.Id,
                            IscrizioneId = new ObjectId(),
                            DataIscrizione = DateTime.Now,
                            CorsoRIF = _repoCorso.GetById(idCorso),
                            UtenteRIF = temp
                        };
                        temp.ElencoIscrizioni.Add(newSub);
                        _repoIscrizione.Insert(newSub);
                        return Redirect("/Utente/MyCourses");
                    }
                    else {
                        var findCorso = _repoIscrizione.GetAll()
                            .Where(i => i.CorsoId == idCorso)
                            .Where(i => i.UtenteId == temp.Id)
                            .FirstOrDefault();

                        if (findCorso == null) {

                            Iscrizione newSub = new Iscrizione() {
                                CorsoId = idCorso,
                                UtenteId = temp.Id,
                                IscrizioneId = new ObjectId(),
                                DataIscrizione = DateTime.Now,
                                CorsoRIF = _repoCorso.GetById(idCorso),
                                UtenteRIF = temp
                            };
                            temp.ElencoIscrizioni.Add(newSub);
                            _repoIscrizione.Insert(newSub);
                            return Redirect("/HOME/");
                        }
                        //L'utente è già iscritto al corso
                        else
                            return Redirect("/HOME/");
                    }
                }
                return Redirect("/HOME/");
            }



        }
    }
} 