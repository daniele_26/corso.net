﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Back_End_GYM.Models {
    public class Utente
    {
        //DATI UTENTE 

        [BsonElement("utenteId")]
        public ObjectId Id { get; set; }

        [BsonElement("nome")]
        [Required(ErrorMessage = "Il campo Nome è obbligatorio")]
        [MaxLength(100)]
        public string Nome { get; set; }
        
        [BsonElement("cognome")]
        [Required(ErrorMessage = "Il campo Cognome è obbligatorio")]
        [MaxLength(100)]
        public string Cognome { get; set; }
        
        [BsonElement("indirizzo")]
        [MaxLength(100)]
        public string Indirizzo { get; set; }

        [BsonElement("dataNascita")]
        [BsonDateTimeOptions(DateOnly = true)]
        [MaxLength(100)]
        private DateTime dataNascita;

        [BsonIgnore]
        public DateTime DataNascita
        {
            get { return dataNascita; }
            set { dataNascita = (DateTime)value; }
        }

        //Controllo email----------------------------------------------------------
        [BsonElement("email")]
        [Required(ErrorMessage = "Il campo Email è obbligatorio")]
        [RegularExpression(@"^[^@\s] +@[^@\s]+\.[^@\s]+$", ErrorMessage = "L'Email deve rispettare il formato corretto")]
        [MaxLength(100)]
        public string Email { get; set; }

        //Controllo password----------------------------------------------------------
        [BsonElement("password")]
        [Required(ErrorMessage = "La password deve essere lunga minimo 6 caratteri")]
        [MaxLength(100),MinLength(6)]
        public string Password { get; set; }

        [BsonIgnore]
        private List<Iscrizione> elencoIscrizioni = new List<Iscrizione>();
        [BsonIgnore]
        public List<Iscrizione> ElencoIscrizioni
        {
            get { return elencoIscrizioni; }
            set { elencoIscrizioni = value; }
        }

}
}
