import React, { useState } from 'react'

export default function Counter( ) {
    
    const[increment,setIncrement]= useState(3);
    const[decrement,setDecrement]= useState(50);

    const clickIncrement = () =>{
        setIncrement (increment+3)
    };

    const clickDecrement = () =>{
        setDecrement (decrement-4)
    };


  return (
    <div>Counter
        <div>
            <p>
                {increment}
            </p>

            <p>
                {decrement}
            </p>
        </div>
        <div>
            <button onClick={clickIncrement}>
                Moltiplica
            </button>

            <button onClick={clickDecrement}>
                Dividi
            </button>

        </div>
    </div>

  )
}
