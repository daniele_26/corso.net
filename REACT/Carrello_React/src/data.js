const list = [
  {
    id: 1,
    title: "Pizza Margherita",
    price: 5,
    img: "https://www.buttalapasta.it/wp-content/uploads/2017/11/pizza-margherita.jpg",
    amount: 1,
  },
  {
    id: 2,
    title: "Amatriciana",
    price: 6,
    img: "https://www.buttalapasta.it/wp-content/uploads/2020/04/bucatini-amatriciana.jpg",
    amount: 1,
  },
  {
    id: 3,
    title: "Lasagne alla bolognese",
    price: 10,
    img: "https://wips.plug.it/cips/buonissimo.org/cms/2018/12/Lasagne-alla-bolognese-4.jpg",
    amount: 1,
  },
  {
    id: 4,
    title: "Spaghetti aglio e olio",
    price: 4,
    img: "https://www.ilgiornaledelcibo.it/wp-content/uploads/2019/07/spaghetti-aglio-olio-peperoncino-876x451.jpg",
    amount: 1,
  },
  {
    id: 5,
    title: "Spaghetti al sugo",
    price: 3,
    img: "https://a-modo-mio.at/wp-content/uploads/2018/08/2spaghetti1a.jpg",
    amount: 1,
  },
  {
    id: 6,
    title: "Panuozzo gragnano",
    price: 20,
    img: "https://www.gusto.it/wp-content/uploads/2016/10/panuozzo-gragnano-gusto.jpg",
    amount: 1,
  },
];

export default list;
