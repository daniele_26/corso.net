CREATE TABLE Persona(
	personaID INT PRIMARY KEY IDENTITY(1,1),
	codice INT NOT NULL UNIQUE,
	nome VARCHAR(255) NOT NULL,
	cognome VARCHAR(255) NOT NULL,
	data_nascita DATE,
	lingua VARCHAR(250),						-- Tipico di Attore
	luogo_nascita VARCHAR(250),					-- Tipico di Regista
);
CREATE TABLE Film(
	filmID INT PRIMARY KEY IDENTITY(1,1),
	titolo VARCHAR(255) UNIQUE NOT NULL,
	data_prod DATE NOT NULL,
	nazionalita VARCHAR(255),
	lingua VARCHAR(255),
	registaRIF INT,
	FOREIGN KEY (registaRIF) REFERENCES Persona(personaID) ON DELETE NO ACTION,
);

CREATE TABLE Persona_Film(
	filmRIF INT NOT NULL,
	attoreRIF INT NOT NULL,
	FOREIGN KEY (filmRIF) REFERENCES Film(filmID) ON DELETE CASCADE,
	FOREIGN KEY (attoreRIF) REFERENCES Persona(personaID) ON DELETE CASCADE,
	UNIQUE(filmRIF, attoreRIF),
);